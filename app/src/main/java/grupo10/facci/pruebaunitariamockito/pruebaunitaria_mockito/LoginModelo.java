package grupo10.facci.pruebaunitariamockito.pruebaunitaria_mockito;

public class LoginModelo implements Login.Modelo {

    Login.Presentador presentador;

    public LoginModelo(Login.Presentador presentador){
        this.presentador = presentador;
    }

    @Override
    public void validarUsuario(String usuario, String password) {
        if (usuario.equals("Mario") && password.equals("12345")){
            presentador.usuarioValido();
        }else{
            presentador.error();
        }
    }
}
