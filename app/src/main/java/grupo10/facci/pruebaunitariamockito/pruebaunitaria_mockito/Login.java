package grupo10.facci.pruebaunitariamockito.pruebaunitaria_mockito;

public interface Login {
    interface View{
        void usuarioValido();
        void error();
        String getUsuario();
        String getPassword();
    }
    interface Presentador{
        void validarUsuario(String usuario, String password);
        void usuarioValido();
        void error();
    }
    interface Modelo{
        void validarUsuario(String usuario, String password);
    }
}
