package grupo10.facci.pruebaunitariamockito.pruebaunitaria_mockito;

public class LoginPresentador implements Login.Presentador {
    Login.View view;
    Login.Modelo modelo;

    public LoginPresentador (Login.View view){
        this.view = view;
        modelo = new LoginModelo(this);
    }

    @Override
    public void validarUsuario(String usuario, String password) {
        if(view!=null){
            modelo.validarUsuario(usuario, password);
        }
    }

    @Override
    public void usuarioValido() {
        if(view!=null){
            view.usuarioValido();
        }
    }

    @Override
    public void error() {
        if(view!=null){
            view.error();
        }
    }
}
