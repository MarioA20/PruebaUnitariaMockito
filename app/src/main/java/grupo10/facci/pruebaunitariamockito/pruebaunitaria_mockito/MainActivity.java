package grupo10.facci.pruebaunitariamockito.pruebaunitaria_mockito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Login.View{

    EditText editTextUsuario, editTextPassword;
    Button buttonLogin;
    Login.Presentador presentador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextUsuario = (EditText) findViewById(R.id.editTextUsuario);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        presentador =  new LoginPresentador(this);

    }

    @Override
    public void usuarioValido() {
        startActivity(new Intent(MainActivity.this, Main2Activity.class));
    }

    @Override
    public void error() {
        Toast.makeText(this,"Usuario no es válido", Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getUsuario() {
        return editTextUsuario.getText().toString();
    }

    @Override
    public String getPassword() {
        return editTextPassword.getText().toString();
    }
}
