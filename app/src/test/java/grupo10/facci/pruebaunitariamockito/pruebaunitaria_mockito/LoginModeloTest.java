package grupo10.facci.pruebaunitariamockito.pruebaunitaria_mockito;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoginModeloTest {
    @Mock
    Login.Presentador presentador;
    LoginModelo modelo;

    @Before
    public void setUp () throws Exception{
        modelo = new LoginModelo(presentador);
    }

    @Test
    public void shouldSuccesWithValidUserandPass () throws Exception{
        modelo.validarUsuario("Mario","12345");
        verify(presentador).usuarioValido();
    }

    @Test
    public void shouldNotSuccesWithValidUserandPass () throws Exception{
        modelo.validarUsuario("Pepe","hola12345");
        verify(presentador).error();
    }

}