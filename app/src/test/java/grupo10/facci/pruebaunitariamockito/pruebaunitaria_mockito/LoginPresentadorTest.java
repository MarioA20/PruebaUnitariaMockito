package grupo10.facci.pruebaunitariamockito.pruebaunitaria_mockito;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginPresentadorTest {

    @Mock
    private Login.View view;
    @Mock
    private Login.Modelo modelo;

    private LoginPresentador presentador;

    @Before
    public void preparar() throws Exception{
        presentador = new LoginPresentador(view);
    }

    @Test
    public void mensajeErrorUsuarioOPasswordEquivocados() throws Exception{
        when(view.getUsuario()).thenReturn("Mario");
        when(view.getPassword()).thenReturn("123456789");
        presentador.validarUsuario(view.getUsuario(),view.getPassword());
        verify(view).error();
    }

    @Test
    public void mensajeUsuarioPasswordCorrectos() throws Exception{
        when(view.getUsuario()).thenReturn("Mario");
        when(view.getPassword()).thenReturn("12345");
        presentador.validarUsuario(view.getUsuario(),view.getPassword());
        verify(view).usuarioValido();
    }
}